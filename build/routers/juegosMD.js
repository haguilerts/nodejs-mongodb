"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importStar(require("express"));
//import {MyJuego} from '../model/modelJuego'
const body_parser_1 = require("body-parser");
//const bodyParser = require('body-parser')
class Juegos {
    constructor() {
        this.router = express_1.Router();
        this.MyJuego = require('../model/modelJuego');
        this.app = express_1.default();
        this.router;
        this.app.use(body_parser_1.urlencoded({ extended: false }));
        this.app.use(body_parser_1.json);
        //this.app.use(bodyParser.urlencoded({extended:false}))
        this.config();
    }
    config() {
        this.router.get("/admin", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                this.arrayJuego = yield this.MyJuego.find();
                console.log('array', this.arrayJuego);
                res.render("admin", { admin: this.arrayJuego });
            }
            catch (error) {
                res.status(404).render("404", { titulo: "Página no encontrado: 404, " });
            }
        }));
        this.router.get("/", (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                this.arrayJuego = yield this.MyJuego.find();
                //console.log('arrayJuego: ',arrayJuego)
                res.render("juegos", { juego: this.arrayJuego });
            }
            catch (error) {
                res.status(404).render("404", { titulo: "Página 404" });
            }
        }));
        this.router.post("/", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const body = req.body;
            console.log(body);
            try {
                /* const creteJuego = new this.MyJuego (body)
                await creteJuego.save()  */
                yield this.MyJuego.create(body);
                res.redirect("/juegosBBDD");
            }
            catch (error) {
                res.status(404).render("404", { titulo: "Página 404" });
            }
        }));
        this.router.get("/:id", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            try {
                const juegoDDBB = yield this.MyJuego.findOne({ _id: id });
                console.log('update/:id: ', juegoDDBB);
                res.render("upJuego", { onejuego: juegoDDBB, error: false });
            }
            catch (error) {
                res.render("upJuego", { titulo: "No se ha encontrado la pag..", error: true });
            }
        }));
        this.router.put("/:id", (req, res) => __awaiter(this, void 0, void 0, function* () {
            const _id = req.params.id;
            const body = req.body;
            console.log('body', body, '- id: ', _id);
            try {
                const update = yield this.MyJuego.findByIdAndUpdate(_id, body, { useFindAndModify: false });
                console.log('update/:id: ', update);
                res.json({
                    estado: true,
                    mensaje: 'Actualizado con exitoo!!!'
                });
                //res.render("upJuego", { onejuego:juegoDDBB,error:false});
            }
            catch (error) {
                console.log(error);
                res.json({
                    estado: false,
                    mensaje: 'no se puedo acuralizar',
                });
                //res.render("upJuego", { titulo: "No se ha encontrado la pag..",error:true });
            }
        }));
        this.router.delete('/:id', (req, res) => __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            try {
                const DeleteJuego = yield this.MyJuego.findByIdAndRemove({ _id: id });
                //console.log(DeleteJuego)          
                if (DeleteJuego) {
                    res.json({
                        estado: false,
                        mensaje: 'No se puede eliminar'
                    });
                }
                else {
                    res.json({
                        estado: true,
                        mensaje: 'eliminado!'
                    });
                }
            }
            catch (error) {
                res.render("upJuego", { titulo: "No se ha encontrado la pag..", error: true });
            }
        }));
    }
}
const j = new Juegos();
exports.default = j.router;
